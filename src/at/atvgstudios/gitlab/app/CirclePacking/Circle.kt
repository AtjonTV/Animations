package at.atvgstudios.gitlab.app.CirclePacking

import processing.core.PApplet

/**
 * CirclePacking Animation
 * @author Thomas Obernosterer
 * @since 18.8.5.1
 */
class Circle(private val app:PApplet, val x: Float, val y: Float) {
    var r = 1f
    var growing = true

    fun grow()
    {
        if(growing)
            r += 0.5f
    }

    fun edges(): Boolean
    {
        return (x+r>app.width || x-r<0 || y+r>app.height || y-r<0)
    }

    fun show()
    {
        app.stroke(255)
        app.strokeWeight(2f)
        app.noFill()
        app.ellipse(x,y,r*2,r*2)
    }
}