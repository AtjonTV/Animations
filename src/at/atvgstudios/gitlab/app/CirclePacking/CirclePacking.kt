package at.atvgstudios.gitlab.app.CirclePacking

import processing.core.PApplet

/**
 * CirclePacking Animation
 * @author Thomas Obernosterer
 * @since 18.8.5.1
 */
class CirclePacking : PApplet() {

    private var circles: MutableList<Circle> = ArrayList()

    /**
     * Set window size 640x360
     */
    override fun settings() {
        size(640,360)
    }

    /**
     * Draw circles
     */
    override fun draw() {
        background(0)

        val total = 10
        var count = 0
        var attempts = 0

        while(count < total)
        {
            val newC = newCircle()
            if(newC != null)
            {
                circles.add(newC)
                count++
            }
            attempts++
            if(attempts > 1000)
            {
                noLoop()
                break
            }
        }

        for (c in circles) {
            if (c.growing) {
                if (c.edges()) {
                    c.growing = false
                } else {
                    for (other in circles) {
                        if (c != other) {
                            val d = PApplet.dist(c.x, c.y, other.x, other.y)
                            if (d - 2 < c.r + other.r) {
                                c.growing = false
                                break
                            }
                        }
                    }
                }
            }
            c.show()
            c.grow()
        }
    }

    private fun newCircle(): Circle?
    {
        val x = random(width+0f)
        val y = random(height+0f)
        var valid=true
        for (c in circles)
        {
            val d = dist(x,y,c.x,c.y)
            if(d < c.r)
            {
                valid=false
                break
            }
        }

        if(valid)
            return Circle(this,x,y)
        else
            return null
    }
}