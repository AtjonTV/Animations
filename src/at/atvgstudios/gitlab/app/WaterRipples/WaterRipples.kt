package at.atvgstudios.gitlab.app.WaterRipples

import processing.core.PApplet

/**
 * WaterRipples Animation
 * @author Thomas Obernosterer
 * @since 18.8.5
 */
class WaterRipples : PApplet() {
    private var current = Array(0) { floatArrayOf(0f,1f) }
    private var previous = Array(0) { floatArrayOf(0f,1f)}
    private val dampening = 0.99f

    /**
     * Set window size 600x400
     */
    override fun settings() {
        size(600,400)
    }

    override fun setup() {
        current = Array(width) { FloatArray(height) }
        previous = Array(width) { FloatArray(height) }
    }

    override fun mouseDragged() {
        if(mouseY >= height || mouseY < 0 || mouseX >= width || mouseX < 0)
            return
        previous[mouseX][mouseY] = 500f
    }

    override fun draw() {
        background(0)
        loadPixels()
        for(i in 1 until width-1)
        {
            for (j in 1 until height-1)
            {
                current[i][j] = (
                    previous[i-1][j] +
                    previous[i+1][j] +
                    previous[i][j-1] +
                    previous[i][j+1]) / 2 - current[i][j]
                current[i][j] = current[i][j] * dampening
                val index = i+j*width
                pixels[index] = color(current[i][j])
            }
        }
        updatePixels()

        val temp = previous
        previous = current
        current = temp
    }
}