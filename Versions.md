| Version | Date |
|---------|------|
| [18.7.27](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.27/out/artifacts/Animations/Animations.jar) | 27.07.2018 |
| [18.7.28](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.28/out/artifacts/Animations/Animations.jar) | 28.07.2018 |
| [18.7.28.2](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.28.2/out/artifacts/Animations/Animations.jar) | 28.07.2018 |
| [18.7.29](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.29/out/artifacts/Animations/Animations.jar) | 29.07.2018 |
| [18.7.29.2](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.29.2/out/artifacts/Animations/Animations.jar) | 29.07.2018 |
| [18.7.29.3](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.29.3/out/artifacts/Animations/Animations.jar) | 29.07.2018 |
| [18.7.29.4](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.29.4/out/artifacts/Animations/Animations.jar) | 29.07.2018 |
| [18.7.29.5](https://gitlab.atvg-studios.at/root/Animations/raw/v18.7.29.5/out/artifacts/Animations/Animations.jar) | 29.07.2018 |
| [18.8.5](https://gitlab.atvg-studios.at/root/Animations/raw/v18.8.5/out/artifacts/Animations/Animations.jar) | 05.08.2018 |
| [18.8.5.1](https://gitlab.atvg-studios.at/root/Animations/raw/v18.8.5.1/out/artifacts/Animations/Animations.jar) | 05.08.2018 |
